package ru.cet.webproject.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
@RequiredArgsConstructor

public class MainController {

    @GetMapping("/index")
    public String getMainPage() {
        return "index";
    }

    @GetMapping("/contact")
    public String getContactPage() {
        return "contact";
    }

    @GetMapping("/cabinet")
    public String getCabinetPage() {
        return "cabinet";
    }
}
